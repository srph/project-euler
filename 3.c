/**
 * Problem #3:
 * The prime factors of 13195 are 5, 7, 13 and 29.
 * What is the largest prime factor of the number 600851475143 ?
 * 
 * Uncompiled. untested.
 */

#include <stdio.h>
#define NUMBER 600851475143

 void main()
 {
 	int index;
 	int large;

 	for(index = 0; index <= NUMBER; index++) {
 		if(index % 2 != 0 || index % 3 != 0 || index % 5 != 0 || index % 7 != 0 || index % 11 != 0) {
 			large = index;
 		}
 	}

 	printf("The largest prime factor of %d is %d", NUMBER, large);
 }
