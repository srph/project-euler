/**
 * Problem #6:
 * The sum of the squares of the first ten natural numbers is,
 *
 * 12 + 22 + ... + 10^2 = 385
 * The square of the sum of the first ten natural numbers is,
 * (1 + 2 + ... + 10)^2 = (55)^2 = 3025
 * Hence the difference between the sum of the squares of the first ten natural numbers and the square of the sum is 3025  385 = 2640.
 * Find the difference between the sum of the squares of the first one hundred natural numbers and the square of the sum.
 * 
 * Uncompiled. untested.
 */

#include <stdio.h>
#include <math.h>

void main()
{
	int a, b;
	unsigned long int total = 0;
	unsigned long int square = 0;;
	unsigned long int sum = 0;

	for(a = 1; a <= 100; a++) {
		sum += pow(a, a);
		square += a;
	}

	square = pow(square, square);
	total = sum - square;

	printf("%d", total);
	getchar();
}
