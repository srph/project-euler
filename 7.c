/**
 * Problem #7:
 * By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see that the 6th prime is 13.
 * What is the 10 001st prime number?
 * 
 * Uncompiled. untested.
 */

#include <stdio.h>

 void main()
 {
 	int count = 0;
 	int prime;
 	unsigned long int a, b;

 	for(a = 0; a <= sizeof(a); a++) {
 		if(count == 10001) {
 			prime = a;
 			break;
 		}
 		for(b = 2; b < a; b++) {
 			if(a % b == 0 ) {
 				break;
 			} else {
 				count++;
 			}
 		}
 	}

 	printf("%d", prime);
 	getchar();
 }
