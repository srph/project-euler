/** 
 * Problem #4:
 * A palindromic number reads the same both ways. The largest palindrome made from the product of two 2-digit numbers is 9009 = 91 99.
 * Find the largest palindrome made from the product of two 3-digit numbers.
 * 
 * Uncompiled. untested.
 */

#include <stdio.h>
#include <stdlib.h>

void main()
{
	char n[4];
	int large = 0;
	int sum;
	int a, b;

	for(a = 0; a <= 999; a++) {
		sprintf(n, "%d", a);
		if(n[2] != '') {
			if(n[0] == n[2]) {
				for(b = 0; b <= 2; b++) {
					sum += atoi(n[b]);
				}
				if(a > large) {
					large = a;
				}
			}
		}
	}

	getchar();
}
