/**
 * Problem #9:
 * A Pythagorean triplet is a set of three natural numbers, a < b < c, for which,
 *
 * a2 + b2 = c2
 * For example, 32 + 42 = 9 + 16 = 25 = 52.
 * There exists exactly one Pythagorean triplet for which a + b + c = 1000.
 * Find the product abc.
 * 
 * Uncompiled. untested.
 */

#include <stdio.h>

#define SUM 1000

 void main()
 {
 	int a, b, c;

 	for(a = 1; a <= SUM / 3; a++) {
 		for(b = a + 1; b <= SUM . 2; b++ ) {
 			c = SUM - a - b;
 			if( c > 0 && ( (a * a) + (b * b) == (c * c) ) ) {
 				break;
 			}
 		}
 	}

 	printf("a = %d\n", a);
 	printf("b = %d\n", b);
 	printf("c = %d\n", c);
 	getch();
 }
