/**
 * Problem #2:
 * Each new term in the Fibonacci sequence is generated by adding the previous two terms.
 * By starting with 1 and 2, the first 10 terms will be:
 *
 * 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, ...
 *
 * By considering the terms in the Fibonacci sequence whose values do not exceed four million,
 * find the sum of the even-valued terms
 *
 * Uncompiled. untested.
 */
#include <stdio.h>

void main()
{
	int index;
	int total = 0;
	int n = 1000;

	int first = 0;
	int second = 1;
	int next;

	for(index = 0; index < n; index++) {
		if(index <= 1) {
			next = index;
			total += next;
		} else {
			next = first + second;
			first = second;
			second = next;
			if(index % 2 == 0) {
				total += next + first + second;
			}
		}
	}

	printf("%d", total);
	getch();
}
