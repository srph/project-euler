/**
 * Problem #10:
 * The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.
 * Find the sum of all the primes below two million.
 */

#include <stdio.h>

#define TWO_M 2000000

void main()
{
 	int a, b;
 	int total;

 	// Iterate through two million numbers

	for(a = 1; a <= TWO_M; a++) {

		for(b = 2; b <= a; b++) {

			if(a == 1 || a == 2 || a == 3 || a == 5 || a == 7 || a == 11) {
				break;
			}

 			if(a % b == 0) {
				break;
			} else {
				total += a;
				break;
			}

		}

	}

 	printf("%d", total);
 	getchar();
}
